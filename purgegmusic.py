from gmusicapi import Mobileclient

username = input('gmusic username: ')
password = input('gmusic password: ')

client = Mobileclient()
client.login(username, password, Mobileclient.FROM_MAC_ADDRESS)
songs = client.get_all_songs()
print('Songs to be deleted:')
toDelete = []
for song in songs:
    try:
        if song['rating'] == '1':
            print('  ' + song['title'] + ' - ' + song['artist'])
            toDelete.append(song['id'])
    except KeyError:
       None

if len(toDelete) < 1:
    print('No disliked songs in your collection.')
    exit(0)

youSure = input('Delete those %s songs? (y/n)' % len(toDelete))
if youSure == 'y' or youSure == 'Y':
    client.delete_songs(toDelete)
    print('Deleted')
else:
    print('No song deleted')