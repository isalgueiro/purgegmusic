# purgegmusic

Deletes all _disliked_ songs from your Google Music collection.

## build

Set up the environment

```shell
virtualenv -p python3 .
. bin/activate
pip install -r requirements.txt
```
